"""
File containing every parameter in the Finding Manifold Holes code.

Parameters

x_col: name of the x coordinates column in the markers file
y_col: name of the y coordinates column in the markers file
z_col: name of the z coordinates column in the markers file
"""

__author__ = 'Marco Paciscopi'



x_col = '//X'
y_col = 'Y'
z_col = 'Z'
name_col = 'name'
red_col = 'R'
green_col = 'G'
blue_col = 'B'
v1_col ='v1'
v2_col ='v2'
v3_col ='v3'
max_area_col = 'max_area'
robust_iter = 5
weight_threshold = 1e-4
