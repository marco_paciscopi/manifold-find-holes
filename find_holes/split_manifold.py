"""
Script that splits the manifold point cloud in slices
"""

__author__ = 'Marco Paciscopi'



import numpy as np
import os
import sys
import Image
import glob
from pyparsing import Word, nums, restOfLine, Suppress, Group, Combine, Optional
import timeit
import math
from itertools import combinations, chain
from scipy.misc import comb
from scipy import special
import cv2
import re
import tables
import argparse
from progressbar import *
import pandas as pd
from bcfind.volume import *
from bcfind.semadec import imtensor
import mahotas as mh
from libtiff import TIFF
import parameters
from bcfind.semadec import imtensor

from scipy.ndimage.filters import gaussian_filter,correlate

def split_manifold(data_file, outdir):


    data_frame = pd.read_csv(data_file)
    X = data_frame.as_matrix([parameters.x_col, parameters.y_col, parameters.z_col])
    name_col =data_frame.as_matrix([parameters.name_col])


    if not os.path.exists(outdir):
                mkdir_p(outdir)


    x_min = min(X[:,0])
    y_min = min(X[:,1])
    z_min = min(X[:,2])
    x_max = max(X[:,0])
    y_max = max(X[:,1])
    z_max = max(X[:,2])


    nz = 15
    depth = y_max - y_min
    margin = 60
    d = int(round(float(depth - margin)/float(nz)))

    for z in xrange(nz):
        zb = d*z + y_min
        za = max(zb - margin, y_min)
        zc = min(depth, zb+d)
        zd = min(zc + margin, depth)

        print za,zb,zc,zd


        submanifold_overlap_a=X[np.logical_and(X[:,1] >= za,X[:,1] < zb)]
        submanifold=X[np.logical_and(X[:,1] >= zb,X[:,1] < zc)]
        submanifold_overlap_b=X[np.logical_and(X[:,1] >= zc,X[:,1] < zd)]
        full_submanifold = np.vstack((submanifold_overlap_a,submanifold, submanifold_overlap_b))

        name_col_submanifold_overlap_a =name_col[np.logical_and(X[:,1] >= za,X[:,1] < zb)]
        name_col_submanifold =name_col[np.logical_and(X[:,1] >= zb,X[:,1] < zc)]
        name_col_submanifold_overlap_b =name_col[np.logical_and(X[:,1] >= zc,X[:,1] < zd)]
        name_col_full_submanifold = np.vstack((name_col_submanifold_overlap_a, name_col_submanifold,name_col_submanifold_overlap_b))
        name_col_full_submanifold = name_col_full_submanifold.reshape(len(name_col_full_submanifold))



        red_col = np.zeros(full_submanifold.shape[0]).astype(int)
        green_col = np.zeros(full_submanifold.shape[0]).astype(int)
        blue_col = np.zeros(full_submanifold.shape[0]).astype(int)
        red_col[0:submanifold_overlap_a.shape[0]]=255
        green_col[submanifold_overlap_a.shape[0]:submanifold.shape[0] + submanifold_overlap_a.shape[0]]=255
        blue_col[submanifold.shape[0] + submanifold_overlap_a.shape[0]:]=255


        full_submanifold_df = pd.DataFrame(data={parameters.x_col: full_submanifold[:, 0],
            parameters.y_col: full_submanifold[:, 1],
            parameters.z_col: full_submanifold[:, 2],
            parameters.red_col: red_col,
            parameters.green_col: green_col,
            parameters.blue_col: blue_col,
            parameters.name_col: name_col_full_submanifold})
        full_submanifold_df.to_csv(outdir +'/'+str(z)+'_'+str(int(zb))+'_'+str(int(zc))+ '.csv', index=False)






def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('data_file', metavar='data_file', type=str,
                        help='csv data file of the whole manifold point cloud')
    parser.add_argument('outdir', metavar='outdir', type=str,
                        help='output folder where the split slices will be saved')
    return parser



def main(args):
    split_manifold(args.data_file, args.outdir)


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)




