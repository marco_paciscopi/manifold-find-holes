#!/usr/bin/env python

"""
Scripts that draws a 3D triangular mesh raster in a 3D volume tensor
"""

__author__ = 'Marco Paciscopi'



import numpy as np
import os
import sys
import Image
import cv2
import argparse
import pandas as pd
from bcfind.volume import *
from bcfind.semadec import imtensor
from libtiff import TIFF
import parameters
from bcfind.semadec import imtensor
from bcfind.semadec import gfilter



def read_from_ply(meshfile):

    id_row = 0
    id_vertex = 0
    id_face = 0
    with open(meshfile) as f:
        for line in f:
            args_line = line.split()
            args_line = line.split()
            if args_line[0] == "element":
                if args_line[1] == "vertex":
                    num_vertices = int(args_line[2])
                if args_line[1] == "face":
                    num_faces = int(args_line[2])
            if args_line[0] == "end_header":
                row_next_header = id_row+1
                break
            id_row+=1
    vertices = {}
    faces = np.empty((num_faces,4)).astype(int)
    color_vertices={}

    id_row = 0
    with open(meshfile) as f:
        for line in f:
            args_line = line.split()
            if id_row >= row_next_header and id_row < row_next_header + num_vertices: #the header is finished
                    vertices[id_vertex] = np.array(args_line[0:3]).astype(int)
                    color_vertices[id_vertex] = np.array(args_line[3:6],dtype=np.uint8)
                    id_vertex+=1
            if id_row >= row_next_header + num_vertices and id_row < row_next_header + num_vertices + num_faces:
                    faces[id_face]=np.array(args_line[0:4])
                    id_face+=1
            id_row+=1

    return vertices,faces,color_vertices


def extract_points_from_mesh(origin,ss_shape,meshfile):


        vertices, faces, color_vertices= read_from_ply(meshfile)

        def isin(v,origin,ss_shape):
            flag=False
            if v[0] > origin[2] and v[0] < origin[2] + ss_shape[2] -1:
                if v[1] > origin[1] and v[1] < origin[1] + ss_shape[1] -1:
                    if v[2] > origin[0] and v[2] < origin[0] + ss_shape[0] -1:
                        flag=True
            return flag

        visible_vertices = {k:v for (k,v) in vertices.items() if isin(v,origin,ss_shape)}


        line_list={}
        for face in faces:
            if face[1] in visible_vertices and face[2] in visible_vertices:
                line_list[tuple((face[1],face[2]))]=np.array([visible_vertices[face[1]][0],visible_vertices[face[1]][1],visible_vertices[face[1]][2],
                                 visible_vertices[face[2]][0],visible_vertices[face[2]][1],visible_vertices[face[2]][2]]).astype(int)
            if face[1] in visible_vertices and face[3] in visible_vertices:
                line_list[tuple((face[1],face[3]))]=np.array([visible_vertices[face[1]][0],visible_vertices[face[1]][1],visible_vertices[face[1]][2],
                                 visible_vertices[face[3]][0],visible_vertices[face[3]][1],visible_vertices[face[3]][2]]).astype(int)
            if face[2] in visible_vertices and face[3] in visible_vertices:
                line_list[tuple((face[2],face[3]))]=np.array([visible_vertices[face[2]][0],visible_vertices[face[2]][1],visible_vertices[face[2]][2],
                                 visible_vertices[face[3]][0],visible_vertices[face[3]][1],visible_vertices[face[3]][2]]).astype(int)


        return line_list,visible_vertices,faces,color_vertices


def bresenham3d(x0,y0,z0,x1,y1,z1):

    path_line=list()
    path_line.append(np.array([x0,y0,z0]).astype(int))
    xf=x1
    yf=y1
    zf=z1

    steepXY = abs(y1 - y0) > abs(x1 - x0)
    if (steepXY):
        x0,y0 = y0,x0
        x1,y1 = y1,x1

    steepXZ = abs(z1 - z0) > abs(x1 - x0)
    if (steepXZ):
        x0,z0 = z0,x0
        x1,z1 = z1,x1


    deltaX = abs(x1 - x0);
    deltaY = abs(y1 - y0);
    deltaZ = abs(z1 - z0);

    errorXY = deltaX / 2
    errorXZ = deltaX / 2;

    stepX = -1 if x0 > x1 else 1
    stepY = -1 if y0 > y1 else 1
    stepZ = -1 if z0 > z1 else 1

    y=y0
    z=z0

    for x in xrange(x0,x1,stepX):
        xCopy=x
        yCopy=y
        zCopy=z

        if steepXZ:
            xCopy,zCopy=zCopy,xCopy
        if steepXY:
            xCopy,yCopy=yCopy,xCopy

        path_line.append(np.array([xCopy,yCopy,zCopy]).astype(int))

        errorXY -= deltaY
        errorXZ -= deltaZ

        if (errorXY < 0):
            y += stepY
            errorXY += deltaX

        if (errorXZ < 0):
            z += stepZ
            errorXZ += deltaX

    path_line.append(np.array([xf,yf,zf]).astype(int))
    path_line=np.array(path_line).T
    return path_line


def draw_mask_line_array(path_line_dict,origin,ss_shape,color_vertices):

    mask_line_array_rgb =  np.zeros((3,ss_shape[0],ss_shape[1],ss_shape[2])).astype(np.uint8)
    for k,v in path_line_dict.items():
        rel_v = v.T - np.array([origin[2],origin[1],origin[0]])
        len_path = rel_v.shape[0]
        rgb1 = color_vertices[k[0]]
        rgb2 = color_vertices[k[1]]


        mask_line_array_rgb[0,rel_v[0:len_path/2,2], rel_v[0:len_path/2,1], rel_v[0:len_path/2,0]]=rgb1[0]
        mask_line_array_rgb[1,rel_v[0:len_path/2,2], rel_v[0:len_path/2,1], rel_v[0:len_path/2,0]]=rgb1[1]
        mask_line_array_rgb[2,rel_v[0:len_path/2,2], rel_v[0:len_path/2,1], rel_v[0:len_path/2,0]]=rgb1[2]

        mask_line_array_rgb[0,rel_v[len_path/2:,2], rel_v[len_path/2:,1], rel_v[len_path/2:,0]]=rgb2[0]
        mask_line_array_rgb[1,rel_v[len_path/2:,2], rel_v[len_path/2:,1], rel_v[len_path/2:,0]]=rgb2[1]
        mask_line_array_rgb[2,rel_v[len_path/2:,2], rel_v[len_path/2:,1], rel_v[len_path/2:,0]]=rgb2[2]

    return mask_line_array_rgb

def smooth_mask_line(mask_line_array_rgb,path_line_dict):

    smoothed_mask_line_array = np.zeros(mask_line_array_rgb.shape).astype(np.uint8)

    for ch_id in xrange(mask_line_array_rgb.shape[0]):

        ch = mask_line_array_rgb[ch_id]

        max_value = np.max(ch)

        num_pix = len(ch[ch > 0])
        line_pixels=np.empty((num_pix,3)).astype(int)
        line_pixels[:,0]= np.where(ch > 0)[0]
        line_pixels[:,1]= np.where(ch > 0)[1]
        line_pixels[:,2]= np.where(ch > 0)[2]

        ch  = gfilter.gaussian_filter(ch, sigma=0.5,mode='constant', cval=0.0)


        num_pix = len(ch[ch > 0])
        query_pixels=np.empty((num_pix,3)).astype(int)
        query_pixels[:,0]= np.where(ch > 0)[0]
        query_pixels[:,1]= np.where(ch > 0)[1]
        query_pixels[:,2]= np.where(ch > 0)[2]



        line_kdtree = cKDTree(line_pixels)
        dist, query_indices = line_kdtree.query(query_pixels,k=2)


        for a in xrange(dist.shape[0]):
            if dist[a,0]>1.0:
                ch[query_pixels[a,0],query_pixels[a,1],query_pixels[a,2]]=0

        num_pix = len(ch[ch > 0])
        query_pixels=np.empty((num_pix,3)).astype(int)
        query_pixels[:,0]= np.where(ch > 0)[0]
        query_pixels[:,1]= np.where(ch > 0)[1]
        query_pixels[:,2]= np.where(ch > 0)[2]


        ch[query_pixels[:,0],query_pixels[:,1],query_pixels[:,2]]+=max_value-np.max(ch)

        num_pix = len(ch[ch > 0])

        smoothed_mask_line_array[ch_id]=ch

    return smoothed_mask_line_array



def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('indir', metavar='indir', type=str,
                        help='needs indir/info.json, substacks, e.g. indir/000, and ground truth, e.g. indir/000-GT.marker')
    parser.add_argument('tensorimage', metavar='tensorimage', type=str,
                        help='HDF5 file containing the whole stack')
    parser.add_argument('substack_id', metavar='substack_id', type=str,
                        help='substack identifier, e.g. 010608')
    parser.add_argument("meshfile", help="3d mesh file (PLY format)")
    parser.add_argument('-o', '--offset', dest='offset', type=int, default=0, help='Offset of the substack that will be extracted from tensorimage')
    parser.add_argument('outdir', metavar='outdir', type=str,
                        help='folder where the output 3D tensor will be saved')
    return parser



#@profile
def main(args):

    ss = SubStack(args.indir, args.substack_id)
    ss.load_volume(args.tensorimage)
    offset=args.offset
    real_tensor,_ = imtensor.load_nearby(args.tensorimage, ss, offset)
    X0,Y0,Z0 = ss.info['X0'], ss.info['Y0'], ss.info['Z0']
    origin = (Z0, Y0, X0)
    H,W,D = ss.info['Height'], ss.info['Width'], ss.info['Depth']
    ss_shape = (D,H,W)

    origin = (Z0-offset, Y0-offset, X0-offset)
    ss_shape= (D+2*offset,H+2*offset,W+2*offset)

    line_list,visible_vertices,_,color_vertices = extract_points_from_mesh(origin,ss_shape,args.meshfile)


    if not line_list:
        print 'There is no mesh edge in substack ' + args.substack_id
        sys.exit(1)
    else:
        print 'Found '+str(len(line_list.keys()))+' mesh edges in substack ' + args.substack_id

    path_line_dict={}
    for k,line in line_list.items():
        path_line = bresenham3d(line[0],line[1],line[2],line[3],line[4],line[5])
        path_line_dict[k]=path_line

    mask_line_array_rgb = draw_mask_line_array(path_line_dict,origin,ss_shape,color_vertices)
    #mask_line_array_rgb = smooth_mask_line(mask_line_array_rgb,path_line_dict)

    output_mask_line_array =  np.zeros((3,ss_shape[0],ss_shape[1],ss_shape[2])).astype(np.uint8)
    for ch_id in xrange(output_mask_line_array.shape[0]):

        ch = mask_line_array_rgb[ch_id]
        output_mask_line_array[ch_id]=real_tensor

        nonzero_voxels=ch[ch>0]
        num_pix=len(nonzero_voxels)

        nonempty_pixels=np.empty((num_pix,3)).astype(int)
        nonempty_indices=np.where(ch > 0)
        nonempty_pixels[:,0]= nonempty_indices[0]
        nonempty_pixels[:,1]= nonempty_indices[1]
        nonempty_pixels[:,2]= nonempty_indices[2]

        output_mask_line_array[ch_id][nonempty_pixels[:,0],nonempty_pixels[:,1],nonempty_pixels[:,2]]=nonzero_voxels[:]


    if not os.path.exists(args.outdir):
        mkdir_p(args.outdir)

    for z in xrange(0, D+2*offset, 1):
        pixels_merged = cv2.merge((output_mask_line_array[0,z,:,:], output_mask_line_array[1,z,:,:], output_mask_line_array[2,z,:,:]))
        im = Image.fromarray(pixels_merged)
        im.save(args.outdir + '/slice_' + str(z).zfill(4) + ".tif", 'TIFF')

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)
