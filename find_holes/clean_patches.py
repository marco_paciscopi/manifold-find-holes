#!/usr/bin/env python

"""
Script that removes from the patches with and without borders (respectively located in patches_path and partition_path folders) all the markers that don't belong to the the corresponding manifold slice
"""

__author__ = 'Marco Paciscopi'


import sys
import numpy as np
import pandas as pd
import argparse
import random
from scipy.spatial import cKDTree, Delaunay
import os
from os import listdir
from os.path import isfile, join
import itertools
import parameters

def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("manifold_slice", help="csv data file of a manifold slice")
    parser.add_argument("patches_path", help="input folder of csv files of the manifold patches with borders")
    parser.add_argument("partition_path", help="input folder of csv files of the manifold patches without borders")
    parser.add_argument("cleaned_patches_path", help="output folder of csv files of the cleaned manifold patches with borders")
    parser.add_argument("cleaned_partition_path", help="output folder of csv files of the cleaned manifold patches without borders")

    return parser




def main(args):


    manifold_slice = args.manifold_slice
    patches_path = args.patches_path
    partition_path = args.partition_path
    cleaned_patches_path = args.cleaned_patches_path
    cleaned_partition_path = args.cleaned_partition_path


    patches_data_files = sorted([ f for f in listdir(patches_path) if isfile(join(patches_path,f)) ])
    partition_data_files = sorted([ f for f in listdir(partition_path) if isfile(join(partition_path,f)) ])
    slice_data_frame = pd.read_csv(manifold_slice)
    slice_names = slice_data_frame[parameters.name_col].tolist()

    if not os.path.exists(cleaned_patches_path):
        os.makedirs(cleaned_patches_path)
    if not os.path.exists(cleaned_partition_path):
        os.makedirs(cleaned_partition_path)


    for f,fd in zip(patches_data_files,partition_data_files):
        patch_data_frame = pd.read_csv(join(patches_path,f))
        partition_data_frame = pd.read_csv(join(partition_path,fd))

        patch_data = patch_data_frame.as_matrix([parameters.x_col,parameters.y_col,parameters.z_col])
        partition_data = partition_data_frame.as_matrix([parameters.x_col,parameters.y_col,parameters.z_col])

        partition_dict = dict(zip(partition_data_frame[parameters.name_col], tuple(map(tuple, partition_data))))
        patch_dict = dict(zip(patch_data_frame[parameters.name_col], tuple(map(tuple, patch_data))))


        partition_names_cleaned = set(partition_dict.keys()) & set(slice_names)
        patch_names_cleaned = set(patch_dict.keys())  & set(slice_names)

        print 'patch ',f,' num points: ', len(patch_dict.keys()),' --> ', len(patch_names_cleaned)
        print 'partition ',fd,' num points: ', len(partition_dict.keys()),' --> ', len(partition_names_cleaned)


        partition_dict_cleaned = { i:partition_dict[i] for i in partition_names_cleaned }
        patch_dict_cleaned = { i:patch_dict[i] for i in patch_names_cleaned }

        out_file_patch = open(join(cleaned_patches_path,f),"w")
        out_file_patch.write(parameters.x_col+','+parameters.y_col+','+parameters.z_col+','+parameters.name_col+'\n')
        for k,v in patch_dict_cleaned.items():
            out_file_patch.write(str(int(v[0]))+','+str(int(v[1]))+','+str(int(v[2]))+','+str(k)+'\n')
        out_file_patch.close()

        out_file_partition = open(join(cleaned_partition_path,f),"w")
        out_file_partition.write(parameters.x_col+','+parameters.y_col+','+parameters.z_col+','+parameters.name_col+'\n')
        for k,v in partition_dict_cleaned.items():
            out_file_partition.write(str(int(v[0]))+','+str(int(v[1]))+','+str(int(v[2]))+','+str(k)+'\n')
        out_file_partition.close()



if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)

