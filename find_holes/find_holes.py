#!/usr/bin/env python

"""
Scripts that finds holes in a 2d point cloud
"""

__author__ = 'Marco Paciscopi'



import sys
import numpy as np
import pandas as pd
import argparse
from scipy.spatial import Delaunay
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Queue import PriorityQueue
from collections import defaultdict
import math
import parameters

def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("data_file_2d", help="2d embedded data file")
    parser.add_argument("data_file_3d", help="3d point cloud data file")
    parser.add_argument("outdir", help="output folder where the results will be saved")
    parser.add_argument('-t', '--threshold', dest='threshold', type=int, default=200, help='threshold to discard the large triangles')#
    return parser

def compute_area_tri(vertices):
    return 1/2.0*abs(vertices[0,0]*vertices[1,1] + vertices[1,0]*vertices[2,1] +
                vertices[2,0]*vertices[0,1] - vertices[1,0]*vertices[0,1] -
                vertices[2,0]*vertices[1,1] - vertices[0,0]*vertices[2,1])

def compute_perimeter_tri(vertices):
    return np.linalg.norm(vertices[0]-vertices[1]) + np.linalg.norm(vertices[1]-vertices[2]) + np.linalg.norm(vertices[2]-vertices[0])

def compute_max_side_tri(vertices):
    return max(np.linalg.norm(vertices[0]-vertices[1]), np.linalg.norm(vertices[1]-vertices[2]), np.linalg.norm(vertices[2]-vertices[0]))

def compute_max_angle_tri(vertices):
    first_angle = math.degrees(math.acos(np.dot(vertices[0]-vertices[1], vertices[0]-vertices[2])/(np.linalg.norm(vertices[0]-vertices[1])*np.linalg.norm(vertices[0]-vertices[2]))))
    second_angle = math.degrees(math.acos(np.dot(vertices[1]-vertices[0], vertices[1]-vertices[2])/(np.linalg.norm(vertices[1]-vertices[0])*np.linalg.norm(vertices[1]-vertices[2]))))
    third_angle = math.degrees(math.acos(np.dot(vertices[2]-vertices[0], vertices[2]-vertices[1])/(np.linalg.norm(vertices[2]-vertices[0])*np.linalg.norm(vertices[2]-vertices[1]))))
    return max(first_angle,second_angle,third_angle)


def compute_area_poly(poly,tri_area):
    return np.sum([tri_area[t] for t in poly])

def get_list_tri_neighbor(tri,closed_tri):
    '''Get the list of
    the neighbored triangles of
    the polytope closed_tri
    '''

    neighbors_set = set()
    for tri_item in closed_tri:

        neighbors_set = neighbors_set | set(tri.neighbors[tri_item])
    return list(neighbors_set)


#@profile
def build_poly_path(path,tri,item,points, path_sign_dict):
    '''Update the path of vertices of a polytope
    and check its convexity
    '''

    is_convex=True
    if not path:
        path = tri.simplices[item].tolist()
        previous_side = points[path[1]]-points[path[0]]
        next_side = points[path[2]]-points[path[1]]
        path_sign_dict[str(path)] = np.sign(previous_side[0]*next_side[1] - next_side[0]*previous_side[1])
        return path,is_convex,path_sign_dict

    found=False
    i=0
    updated_path=path
    while not found and i<len(path):
        side=[path[i % len(path)],path[(i+1) % len(path)]]
        if set(side) < set(tri.simplices[item].tolist()):
            updated_path=path[0:i+1]+ list(set(tri.simplices[item].tolist()) - set(side))  + path[i+1:]
            len_path = len(updated_path)
            previous_side = points[updated_path[i % len_path]]-points[updated_path[(i-1) % len_path]]
            new_side_1 = points[updated_path[(i+1) % len_path]]-points[updated_path[i % len_path]]
            new_side_2 = points[updated_path[(i+2) % len_path]]-points[updated_path[(i+1) % len_path]]
            next_side = points[updated_path[(i+3) % len_path]]-points[updated_path[(i+2) % len_path]]
            sign_cross_product_1 = np.sign(previous_side[0]*new_side_1[1] - new_side_1[0]*previous_side[1])
            sign_cross_product_2 = np.sign(new_side_1[0]*new_side_2[1] - new_side_2[0]*new_side_1[1])
            sign_cross_product_3 = np.sign(new_side_2[0]*next_side[1] - next_side[0]*new_side_2[1])
            if not sign_cross_product_1 ==  sign_cross_product_2 == sign_cross_product_3 == path_sign_dict[str(path)]:
                is_convex=False
            else:
                path_sign_dict[str(updated_path)]=sign_cross_product_1
            found=True
        i+=1
    return updated_path,is_convex,path_sign_dict


def get_max_poly(max_poly,poly,tri_area):
    if compute_area_poly(poly,tri_area) > compute_area_poly(max_poly,tri_area):
        return poly
    else:
        return max_poly



def create_color_mesh(points_3d, tri_simplices, good_tri_set, color_face_vector,outfile):
    '''Create a colored 3d mash in PLY format
    from a set of faces and triangles
    '''

    ply = open(outfile, 'w')
    num_vertices = len(points_3d)
    num_faces = len(good_tri_set)

    color_vector = np.zeros(points_3d.shape[0])
    for triang,item in zip(tri_simplices,np.arange(tri_simplices.shape[0])):
            if item in good_tri_set:
                color_vector[triang[0]]=max(color_vector[triang[0]],color_face_vector[item])
                color_vector[triang[1]]=max(color_vector[triang[1]],color_face_vector[item])
                color_vector[triang[2]]=max(color_vector[triang[2]],color_face_vector[item])


    color_map_type = plt.get_cmap('jet')#choose the colormap function ('jet','hot',...)
    color_vector = (color_vector - min(color_vector)) / (max(color_vector) - min(color_vector))#normalization
    color_map = color_map_type(color_vector)
    color_map = np.round(color_map*255).astype(np.uint8)
    header = 'ply\nformat ascii 1.0\nelement vertex '+str(num_vertices)+'\nproperty float x\nproperty float y\nproperty float z\n' + \
            'property uchar red\nproperty uchar green\nproperty uchar blue\n'+ \
            'element face '+str(num_faces)+'\nproperty list uchar int vertex_indices\nend_header\n'
    ply.write(header)
    for pt,color_triple in zip(points_3d,color_map):
        ply.write(str(pt[0])+' '+str(pt[1])+' '+str(pt[2])+' '+str(color_triple[0])+' '+str(color_triple[1])+' ' +str(color_triple[2])+'\n')


    n_vert_per_face = len(tri_simplices[0])
    for item  in good_tri_set:
        ply.write(str(n_vert_per_face)+' '+str(tri_simplices[item][0])+' '+str(tri_simplices[item][1])+' '+str(tri_simplices[item][2])+'\n')

    ply.close()


def write_results(points_2d, tri, good_tri_set, color_face_vector, name_points, outdir, filename):
    '''Write the file of vertices and off the triangles
    '''

    num_vertices = len(points_2d)
    num_faces = len(good_tri_set)

    color_vector = np.zeros(points_2d.shape[0])
    for triang,item in zip(tri.simplices,np.arange(tri.simplices.shape[0])):
            if item in good_tri_set:
                color_vector[triang[0]]=max(color_vector[triang[0]],color_face_vector[item])
                color_vector[triang[1]]=max(color_vector[triang[1]],color_face_vector[item])
                color_vector[triang[2]]=max(color_vector[triang[2]],color_face_vector[item])

    max_area_df = pd.DataFrame(data={'name': name_points.reshape(num_vertices),
            'max_area': color_vector})

    tri_set = name_points[tri.simplices]
    tri_set = tri_set[list(good_tri_set)].reshape((len(good_tri_set),3))

    tri_df = pd.DataFrame(data={'v1': tri_set[:, 0],
                            'v2': tri_set[:, 1],
                            'v3': tri_set[:, 2]})


    outdir_vertices = outdir + '/vertices'
    outdir_tri = outdir + '/tri'

    if not os.path.exists(outdir_vertices):
        os.makedirs(outdir_vertices)
    if not os.path.exists(outdir_tri):
        os.makedirs(outdir_tri)

    max_area_df.to_csv(outdir_vertices +'/'+filename, index=False)
    tri_df.to_csv(outdir_tri + '/'+filename, index=False)



def compute_shape(tri,points,min_threshold,max_threshold):
    '''Remove from outside to inside the faces of the Delaunay Triangulation
    whose perimeter is below the min_threshold value or overt the max_threshold value
    '''

    tri_objects = points[tri.simplices]
    tri_colors= np.zeros(shape=(tri_objects.shape[0])).astype(np.float)
    tri_neighbors = tri.neighbors


    tri_found = True
    neighbors = dict(zip(np.arange(len(tri.simplices)), np.array([len(tri_neighbors[tri_neighbors[i,:]>0]) for i in xrange(tri_neighbors.shape[0])])))

    removed_simplices = list()
    while tri_found is True:
        list_boundary = [key for key, value in neighbors.items() if value < 3]
        tri_found = False
        for i in list_boundary:
                if i not in removed_simplices:
                    perimeter=compute_perimeter_tri(tri_objects[i,:])
                    if perimeter<min_threshold  or  perimeter>max_threshold:
                        good_neighbors = np.where(tri_neighbors[i] >= 0)[0]
                        for j in xrange(len(good_neighbors)):
                            neighbors[tri_neighbors[i][good_neighbors[j]]]-=1
                        tri_found = True
                        removed_simplices.append(i)
    tri_colors[removed_simplices]=1

    good_indices = np.logical_and(tri_colors != 1,True)

    return good_indices

def main(args):
    data_file_2d = args.data_file_2d
    data_file_3d= args.data_file_3d
    outdir = args.outdir


    max_threshold = args.threshold


    data_frame_2d = pd.read_csv(data_file_2d)
    data_frame_3d = pd.read_csv(data_file_3d)


    if not os.path.exists(outdir):
        os.makedirs(outdir)


    points_2d = data_frame_2d.as_matrix([parameters.x_col, parameters.y_col])
    points_3d = data_frame_3d.as_matrix([parameters.x_col, parameters.y_col,parameters.z_col])

    tri = Delaunay(points_2d,furthest_site=False,incremental=False)

    tri_objects = points_2d[tri.simplices]
    tri_area = np.zeros(shape=(tri_objects.shape[0])).astype(np.float)
    tri_perimeter = np.zeros(shape=(tri_objects.shape[0])).astype(np.float)
    tri_max_angle = np.zeros(shape=(tri_objects.shape[0])).astype(np.float)
    for i in xrange(tri_area.shape[0]):
        tri_area[i] = compute_area_tri(tri_objects[i,:])
        tri_perimeter[i] = compute_perimeter_tri(tri_objects[i,:])
        tri_max_angle[i] = compute_max_angle_tri(tri_objects[i,:])
        tri_num_neighbors[i] = len(np.where(tri.neighbors[i]>=0)[0])

    good_indices = compute_shape(tri,points_2d,0,max_threshold)


    #uncomment these lines to show the discarded triangles
    #matplotlib.pyplot.switch_backend('WXAgg')
    #fig = plt.figure()
    #plt.tripcolor(points_2d[:,0], points_2d[:,1], tri.simplices[good_indices], facecolors=tri_area[good_indices], edgecolors='k',alpha=1.0)
    #plt.show()
    #sys.exit(0)


    available_set = set(np.arange(len(good_indices))[good_indices].tolist())
    holes=np.zeros(tri_area.shape)
    holes_path={}
    maximal_polytopes = {}
    polytopes_triangle = defaultdict(list)
    iter=1
    path_sign_dict= {}
    max_iter_poly = 10000
    for item in available_set:  #search the largest polytope for every tringle of the Delauney triangulation
        poly_root = [1.0/tri_area[item], set([item])]
        open_poly = PriorityQueue()
        open_poly.put(poly_root)
        open_path_dict = dict()
        open_path_dict[str([item])], _, path_sign_dict = build_poly_path([],tri,item,points_2d,path_sign_dict)
        max_poly = set([item])
        closed_poly=list()
        iter_poly=0
        while not open_poly.empty():    #visit all the open polytopes stored in the priory queue open_poly until the the queue is empty
        #while not open_poly.empty() and iter_poly < max_iter_poly:
            node = open_poly.get()
            poly=node[1]
            if poly in closed_poly:
                continue
            expandable = False
            index_poly = sorted(poly)
            path = open_path_dict[str(index_poly)]

            for neighbor in get_list_tri_neighbor(tri,poly):
                poly_temp = poly.union(set([neighbor]))
                index_poly_temp = sorted(poly_temp)
                path_temp, is_convex, path_sign_dict =  build_poly_path(path, tri, neighbor, points_2d, path_sign_dict)
                if index_poly_temp  not in  closed_poly and is_convex and  good_indices[neighbor] and path_temp != path :
                    if str(index_poly_temp)  not in open_path_dict:
                        expandable = True
                        open_poly.put([1.0/compute_area_poly(poly_temp,tri_area),poly_temp])
                        open_path_dict[str(index_poly_temp)] = path_temp

            if not expandable:
                max_poly = get_max_poly(max_poly,poly,tri_area)
                max_poly_path = open_path_dict[str(sorted(max_poly))]
                closed_poly.append(index_poly)
            iter_poly+=1
        print 'triangle '+str(iter) + '/' + str(len(available_set)) + ' (visited ' + str(iter_poly)+ ' polytopes)'
        iter+=1
        maximal_polytopes[item] = max_poly

        for _tri in max_poly:
            polytopes_triangle[_tri].append(max_poly)

        holes_path[item]=max_poly_path

    unique_maximal_polytopes = set([tuple(sorted(p)) for p in maximal_polytopes.values()])
    unique_maximal_polytopes = {p:np.sum(tri_area[list(p)]) for p in unique_maximal_polytopes}
    for triangle in available_set:
        holes[triangle] = max([ np.sum(tri_area[list(p)]) for p in polytopes_triangle[triangle]])


    outdir_holes_results = outdir + '/holes_results'
    outdir_mesh2d_results = outdir + '/mesh2d_results'
    outdir_mesh3d_results = outdir + '/mesh3d_results'
    if not os.path.exists(outdir_holes_results):
        os.makedirs(outdir_holes_results)
    if not os.path.exists(outdir_mesh2d_results):
        os.makedirs(outdir_mesh2d_results)
    if not os.path.exists(outdir_mesh3d_results):
        os.makedirs(outdir_mesh3d_results)

    fig = plt.figure()
    plt.tripcolor(points_2d[:,0], points_2d[:,1], tri.simplices[good_indices], facecolors=holes[good_indices], edgecolors='k',alpha=1.0)
    plt.savefig(outdir_mesh2d_results+'/'+os.path.splitext(os.path.basename(data_file_2d))[0]+'.eps', format='eps', dpi=1000)
    create_color_mesh(points_3d,tri.simplices,available_set, holes, outdir_mesh3d_results +'/'+os.path.splitext(os.path.basename(data_file_2d))[0]+'.ply')
    name_points =data_frame_3d.as_matrix([parameters.name_col])
    write_results(points_2d, tri, available_set, holes, name_points, outdir_holes_results, os.path.splitext(os.path.basename(data_file_2d))[0])

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)
