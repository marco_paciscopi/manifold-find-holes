#!/usr/bin/env python

"""
Scripts that embeds a set of points using an algorithm of non-linear dimensionality reduction (Isomap)
"""

__author__ = 'Marco Paciscopi'



import sys
import os
import numpy as np
import graph_utils
import pandas as pd
import parameters
import argparse
from sklearn.manifold import Isomap

def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("data_file", help="3d point cloud data file")
    parser.add_argument("outdir", help="output folder where the 2d embedded data file will be saved")

    return parser


def main(args):

    data_file = args.data_file
    data_frame = pd.read_csv(data_file)

    outdir = args.outdir
    outdir_embeddings = outdir + '/embeddings/'

    if not os.path.exists(outdir_embeddings):
        os.makedirs(outdir_embeddings)

    X = data_frame.as_matrix([parameters.x_col, parameters.y_col, parameters.z_col])


    n_neighbors_patch = graph_utils.compute_minimum_nearest_neighbors(X)

    iso = Isomap(n_neighbors_patch, 2)

    points_2d = iso.fit_transform(X)


    filename = os.path.splitext(os.path.basename(data_file))[0]
    embed_df = pd.DataFrame(data={parameters.x_col: points_2d[:, 0],
        parameters.y_col: points_2d[:, 1]})
    embed_df.to_csv(outdir_embeddings + '/'+filename+'.csv', index=False)



if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)
