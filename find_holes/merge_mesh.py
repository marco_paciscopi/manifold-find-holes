#!/usr/bin/env python

"""
Scripts that builds a 3D RGB mesh in PLY format (Polygon File Format) from the full set of manifold patches
"""

__author__ = 'Marco Paciscopi'


import sys
import numpy as np
import pandas as pd
import argparse
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math
import parameters
from os import listdir
from os.path import isdir,isfile, join
import itertools

def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('patch_dir', metavar='patch_dir', type=str,
                        help='folder in which the results for all the manifold slices are stored')
    parser.add_argument("outdir", help="folder in which the PLY file of the merged 3D mesh will be saved")
    parser.add_argument('-t', '--threshold', dest='threshold', type=float, default=50000, help='threshold to discard the large triangles')#
    parser.add_argument('-s','--slices', nargs='+', help='IDs of the slices to be merged together', type=str, required=True)

    return parser



def create_color_mesh(points_3d_dict, tri_simplices, color_vertices, color_threshold, outfile):
    ply = open(outfile, 'w')

    for k,v in color_vertices.items():
        if v ==0:
            del color_vertices[k]

    num_vertices = len(color_vertices)

    print '\ntotal num_vertices: ',num_vertices

    color_vertices_keys = color_vertices.keys()

    good_faces=list(set(np.where(pd.Index(pd.unique(color_vertices_keys)).get_indexer(tri_simplices[:,0]) >= 0)[0]) & \
                     set(np.where(pd.Index(pd.unique(color_vertices_keys)).get_indexer(tri_simplices[:,1]) >= 0)[0]) & \
                     set(np.where(pd.Index(pd.unique(color_vertices_keys)).get_indexer(tri_simplices[:,2]) >= 0)[0]))

    num_faces = len(good_faces)
    print 'total num_faces: ',num_faces



    header = 'ply\nformat ascii 1.0\nelement vertex '+str(num_vertices)+'\nproperty float x\nproperty float y\nproperty float z\n' + \
             'property uchar red\nproperty uchar green\nproperty uchar blue\n'+ \
             'element face '+str(num_faces)+'\nproperty list uchar int vertex_indices\nend_header\n'
    ply.write(header)


    color_map_type = plt.get_cmap('jet')#hot
    #color_vertices = {key:math.log(value) for key,value in color_vertices.items()}
    max_color = max(color_vertices.values())
    min_color = min(color_vertices.values())
    color_vertices = {k:min(v,color_threshold) for k,v in color_vertices.items()}
    print 'min_color= '+str(min_color)+' max_color= '+str(max_color)+' threshold= '+str(color_threshold)

    max_color = max(color_vertices.values())
    min_color = min(color_vertices.values())


    dict_vertices = {}
    iter = 0

    for key, value in  color_vertices.iteritems():
        color_map = np.round(np.array(color_map_type((value - min_color) / (max_color - min_color)))*255).astype(np.uint8)
        location = points_3d_dict[key]
        ply.write(str(location[0])+' '+str(location[1])+' '+str(location[2])+' '+str(color_map[0])+' '+str(color_map[1])+' ' +str(color_map[2])+'\n')
        dict_vertices[key]=iter
        iter+=1

    n_vert_per_face = len(tri_simplices[0])
    for id_face  in good_faces:
        ply.write(str(n_vert_per_face)+' '+str(dict_vertices[tri_simplices[id_face][0]])+' '+str(dict_vertices[tri_simplices[id_face][1]])+' '+str(dict_vertices[tri_simplices[id_face][2]])+'\n')


    ply.close()




def main(args):

    patch_dir = args.patch_dir
    outdir = args.outdir
    threshold = args.threshold

    list_slices = args.slices

    n_slices = len(list_slices)
    tri_simplices=list()
    color_vertices_dict = {}
    full_curvature_dict = {}
    points_3d_dict = {}

    if not os.path.exists(outdir):
        os.makedirs(outdir)
    outfile = outdir + '/merged_mesh'


    for z in list_slices:

        partition_path = patch_dir+'/'+z+'/partition_cloud/'
        vertices_path = patch_dir+'/'+z+'/results/holes_results/vertices'
        tri_path = patch_dir+'/'+z+'/results/holes_results/tri'

        print 'reading patches of the ' + z + ' slice...'
        outfile+='_'+z
        vertices_data_files = sorted([ f for f in listdir(vertices_path) if isfile(join(vertices_path,f)) ])
        tri_data_files = sorted([ f for f in listdir(tri_path) if isfile(join(tri_path,f)) ])
        partition_data_files = sorted([ f for f in listdir(partition_path) if isfile(join(partition_path,f)) ])

        for f,fd,ft in zip(vertices_data_files,partition_data_files,tri_data_files):
            vertices_data_frame = pd.read_csv(join(vertices_path,f))
            partition_data_frame = pd.read_csv(join(partition_path,fd))
            tri_data_frame = pd.read_csv(join(tri_path,f))
            tri_s = tri_data_frame.as_matrix([parameters.v1_col, parameters.v2_col, parameters.v3_col]).tolist()
            partition_names = partition_data_frame[parameters.name_col].tolist()
            vertices_dict = dict(zip(vertices_data_frame[parameters.name_col], vertices_data_frame[parameters.max_area_col]))
            vertices_dict_reduced = dict([(i,vertices_dict[i]) for i in partition_names if i in vertices_dict])
            color_vertices_dict.update(vertices_dict_reduced)
            points_3d_dict.update(partition_data_frame.set_index(parameters.name_col).T.to_dict('list'))
            tri_simplices.extend([sorted(row) for row in tri_s])
            tri_simplices.sort()
            tri_simplices=list(k for k,_ in itertools.groupby(tri_simplices))


    tri_simplices = np.array(tri_simplices,dtype=int)

    outfile+='.ply'
    create_color_mesh(points_3d_dict, tri_simplices, color_vertices_dict, threshold, outfile)



if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)

