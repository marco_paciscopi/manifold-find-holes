# -*- coding: utf-8 -*-

"""
Scripts that cuts the margins from all the patches of a single manifold slice
"""

__author__ = 'Marco Paciscopi'



import numpy as np
import os
import sys
import argparse
import pandas as pd
import parameters
from os import listdir
from os.path import isdir,isfile,join



def cut_margins(full_manifold_file, patch_dir, cut_patch_dir, id_slice):


    data_frame = pd.read_csv(full_manifold_file)
    X = data_frame.as_matrix([parameters.x_col, parameters.y_col, parameters.z_col])
    name_col =data_frame.as_matrix([parameters.name_col])


    if not os.path.exists(cut_patch_dir):
        os.makedirs(cut_patch_dir)


    x_min = min(X[:,0])
    y_min = min(X[:,1])
    z_min = min(X[:,2])
    x_max = max(X[:,0])
    y_max = max(X[:,1])
    z_max = max(X[:,2])


    nz = 15
    depth = y_max - y_min
    margin = 60
    d = int(round(float(depth - margin)/float(nz)))

    z=id_slice

    zb = d*z + y_min
    za = max(zb - margin, y_min)
    zc = min(depth, zb+d)
    zd = min(zc + margin, depth)

    print za,zb,zc,zd


    patch_files = [ f for f in listdir(patch_dir) if isfile(join(patch_dir,f)) ]


    for id in xrange(len(patch_files)):
        patch_frame = pd.read_csv(join(patch_dir,patch_files[id]))
        patch = patch_frame.as_matrix([parameters.x_col, parameters.y_col , parameters.z_col, parameters.name_col])
        good_patch = patch[np.logical_and(patch[:,1] >= zb, patch[:,1]<zc )].astype(int)
        print good_patch.shape,patch.shape

        good_patch_df = pd.DataFrame(data={parameters.x_col: good_patch[:, 0],
            parameters.y_col: good_patch[:, 1],
            parameters.z_col: good_patch[:, 2],
            parameters.name_col: good_patch[:,3]})
        good_patch_df.to_csv(join(cut_patch_dir, patch_files[id]), index=False)




def get_parser():
    parser = argparse.ArgumentParser(description="""
    Remove the margins from the patch set of a single manifold slice
    """, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('full_manifold_file', metavar='full_manifold_file', type=str,
                        help='csv data file of the whole manifold point cloud')
    parser.add_argument('patch_dir', metavar='patch_dir', type=str,
                        help='folder of the overlapping patches')
    parser.add_argument('cut_patch_dir', metavar='cut_patch_dir', type=str,
                        help='output folder where the cut patches will be saved')
    parser.add_argument('id_slice', metavar='id_slice', type=int,
                        help='id of the manifold slice from which we want to remove the margins')
    return parser



def main(args):
    cut_margins(args.full_manifold_file, args.patch_dir, args.cut_patch_dir, args.id_slice)


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)





