#!/usr/bin/env bash

id_slice=$1
patch_file=$2

manifold_path=/fast/logos_c/paciscopi/find_holes_results
script_path=$HOME/data/manifold_find_holes/find_holes
slices_path=$manifold_path/results-all-SUP_WNEG_r5_charts_15
slices_work_path=$manifold_path/results-all-SUP_WNEG_r5_charts_15/slices_work/
work_path=$slices_work_path/$id_slice
patches_path=$work_path/patches


printf "\n2d embedding  slice $id_slice patch $patch_file ...\n"
python $script_path/embed.py $patch_file $work_path/results/isomap_results
filename=$(basename $patch_file)
printf "\nfinding holes  slice $id_slice patch $patch_file ...\n"
python $script_path/find_holes.py $work_path/results/isomap_results/embeddings/$filename  $patch_file  $work_path/results
