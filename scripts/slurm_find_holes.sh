#!/usr/bin/env bash



##preprocessing##
manifold_path=$HOME/data/manifold_find_holes
script_path=$manifold_path/find_holes
slices_path=$manifold_path/results-all-SUP_WNEG_r5_charts_15
slices_work_path=$manifold_path/results-all-SUP_WNEG_r5_charts_15/slices_work/

rm -r $manifold_path/slurm_find_holes_log
mkdir -p $manifold_path/slurm_find_holes_log

#for id_slice in {1,2,3,4,5,6,7,8,9,10,11,12,13,14,0}
for id_slice in 13
do   
    work_path=$slices_work_path/$id_slice
    patches_path=$work_path/patches
    echo "updating patches according to the slice $id_slice ..."
    python $script_path/clean_patches.py $work_path/slice_${id_slice}.csv    $work_path/patches $work_path/partition_cloud $work_path/patches/ $work_path/partition_cloud
    echo "removing margins of the patches of the slice $id_slice ..."
    python $script_path/cut_patch_margins.py $slices_path/results-all-SUP_WNEG_r5.csv $work_path/partition_cloud $work_path/partition_cloud $id_slice 
done

################

#for id_slice in {1,2,3,4,5,6,7,8,9,10,11,12,13,14,0}
for id_slice in 13
do   
    work_path=$slices_work_path/$id_slice
    patches_path=$work_path/patches
    for file in $patches_path/*; 
    do
        #sbatch   --job-name=manifold   --cpus-per-task=1  --partition=lopart -D $manifold_folder/slurm_find_holes_log $manifold_path/scripts/find_holes_patch.sh $id_slice $file
        $manifold_path/scripts/find_holes_patch.sh $id_slice $file
    done
done

